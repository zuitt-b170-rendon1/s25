//fruitsOnSale
db.fruits.aggregate(
  [{
    
      $match: {
        onSale: true
        }
      }
    ,
    {
      $count: "fruitsOnSale"
    }
  ]
)

//stock more than 20
db.fruits.aggregate(
  [{
    
      $match: {
        stock: {$gt:20}
        }
      }
    ,
    {
      $count: "enoughStock"
    }
  ]
)

//average operator 

db.fruits.aggregate([
   { $match: { stock: {$gt:10}} } ,
   { $group: { _id: "$supplier_id", avg_price: { $avg: "$price" } } },

]);

// max operator 

db.fruits.aggregate([
   { $match: { stock: {$gt:10}} } ,
   { $group: { _id: "$supplier_id", max_price: { $max: "$price" } } },

]);

// min operator 
db.fruits.aggregate([
   { $match: { stock: {$gt:10}} } ,
   { $group: { _id: "$supplier_id", min_price: { $min: "$price" } } },

]);